// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  coverPageComponentName: 'cover-page',
  firebase: {
    apiKey: 'AIzaSyC9DUN0h9ViP-ErZHpko_Zb-7H8UCzA2og',
    authDomain: 'marketing-quiz-3e390.firebaseapp.com',
    databaseURL: 'https://marketing-quiz-3e390.firebaseio.com',
    projectId: 'marketing-quiz-3e390',
    storageBucket: 'marketing-quiz-3e390.appspot.com',
    messagingSenderId: '153901942415',
    appId: '1:153901942415:web:4b29b6c64711a7425d12fa'
  }
};
