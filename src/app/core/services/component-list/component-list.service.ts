import {Injectable} from '@angular/core';
import {AbstractDynamicList} from '../../abstract/services/abstract-dynamic-list';

@Injectable()
export class ComponentListService extends AbstractDynamicList {
  protected list() {
    return {};
  }
}
