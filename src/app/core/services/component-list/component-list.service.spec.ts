import { TestBed, inject } from '@angular/core/testing';

import { ComponentListService } from './component-list.service';

describe('ComponentListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ComponentListService]
    });
  });

  it('should be created', inject([ComponentListService], (service: ComponentListService) => {
    expect(service).toBeTruthy();
  }));
});
