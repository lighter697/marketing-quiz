import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {QuizService} from '../../modules/quiz/services/quiz/quiz.service';
import {map, tap} from 'rxjs/operators';
import {QuizModel} from '../../modules/quiz/models/quiz.model';
import {ModalConfirmComponent} from '../../shared/components/modal-confirm/modal-confirm.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class EditQuizGuard implements CanActivate {
  constructor(
    private quizService: QuizService,
    private modal: NgbModal,
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkPermission(next);
  }

  private checkPermission(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.quizService.get(route.params['id']).pipe(
      map((quiz: QuizModel) => {
        if (!quiz.published) {
          const modalRef = this.modal.open(ModalConfirmComponent);
          modalRef.componentInstance.title = 'Notice!';
          modalRef.componentInstance.text = 'You can not edit this quiz until it\'s not published';
          modalRef.componentInstance.confirm = 'OK';
        }
        return quiz.published;
      })
    );
  }
}
