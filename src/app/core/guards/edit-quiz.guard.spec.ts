import { TestBed, async, inject } from '@angular/core/testing';

import { EditQuizGuard } from './edit-quiz.guard';

describe('EditQuizGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EditQuizGuard]
    });
  });

  it('should ...', inject([EditQuizGuard], (guard: EditQuizGuard) => {
    expect(guard).toBeTruthy();
  }));
});
