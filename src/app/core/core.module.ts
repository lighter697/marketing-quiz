import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {RouterModule} from '@angular/router';
import {QuizCreateComponent} from '../shared/components/quiz-create/quiz-create.component';

@NgModule({
  entryComponents: [
    QuizCreateComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    HeaderComponent,
  ],
  exports: [
    HeaderComponent,
  ]
})
export class CoreModule {
}
