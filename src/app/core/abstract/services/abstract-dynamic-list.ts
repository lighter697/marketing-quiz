export abstract class AbstractDynamicList {

  protected abstract list();

  getList() {
    return this.list();
  }

  getComponentByAlias(alias: string) {
    const field = this.getList().hasOwnProperty(alias);
    if (!field) {
      const supportedTypes = Object.keys(this.list).join(', ');
      throw new Error(
        `Not found (${alias}).
        Supported types: ${supportedTypes}`
      );
    }

    return this.getList()[alias];
  }
}
