import {Injectable, Input, OnInit} from '@angular/core';
import {BrickDragActionService} from '../../../../modules/quiz/services/brick-drag-action/brick-drag-action.service';
import {InputComponent} from '../../../../modules/dynamic-component-loader/models/input-component';
import {CardModel} from '../../../../modules/quiz/models/card.model';

@Injectable()
export abstract class AbstractBrickComponent implements OnInit {

  @Input() inputComponent: InputComponent<CardModel>;

  constructor(protected brickDragActionService: BrickDragActionService) {
  }

  ngOnInit() {
  }

  drag(event) {
    this.brickDragActionService.startDrag(<InputComponent<CardModel>>{
      component: this.inputComponent.component,
      params: {
        sectionId: this.inputComponent.params.sectionId,
      }
    });
  }

  dragEnd() {
    this.brickDragActionService.endDrag();
  }
}
