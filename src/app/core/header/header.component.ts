import {Component, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {QuizCreateComponent} from '../../shared/components/quiz-create/quiz-create.component';
import {from} from 'rxjs/observable/from';
import {catchError, filter} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {QuizService} from '../../modules/quiz/services/quiz/quiz.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  showNavBar = false;

  constructor(
    private modal: NgbModal,
    private quizService: QuizService,
    private router: Router,
  ) {
  }

  ngOnInit() {

  }

  toggleNavBar() {
    this.showNavBar = !this.showNavBar;
  }

  createQuiz() {
    const modalRef = this.modal.open(QuizCreateComponent);

    return from(modalRef.result).pipe(
      catchError(() => of(false)),
      filter(result => !!result)
    ).subscribe((result) => {
      const quizRef = this.quizService.create(result);
      this.router.navigate(['/']).then();
    });
  }
}
