import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {InputComponent} from '../../models/input-component';
import {ComponentListService} from '../../../../core/services/component-list/component-list.service';

interface DataSchemaInterface {
  [key: string]: any;
}

@Component({
  selector: 'app-dynamic-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements AfterViewInit, OnDestroy, OnChanges, OnInit {

  @Input() inputComponent: InputComponent<any>;
  componentRef: ComponentRef<any>;

  @ViewChild('viewRef', {read: ViewContainerRef}) public viewRef: ViewContainerRef;

  public constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private componentListService: ComponentListService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
  }

  ngAfterViewInit() {
    this.viewRef.clear();
    this.loadComponent(this.viewRef);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.componentRef) {
      this.componentRef.instance.inputComponent = this.inputComponent;
    }
  }

  ngOnInit(): void {

  }

  loadComponent(viewContainerRef: ViewContainerRef) {
    const component = this.componentListService.getComponentByAlias(this.inputComponent.component);
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
    this.componentRef = viewContainerRef.createComponent(componentFactory);
    this.componentRef.instance.inputComponent = this.inputComponent;
    this.componentRef.changeDetectorRef.detectChanges();
  }

  ngOnDestroy() {
    this.changeDetectorRef.detach();
  }
}
