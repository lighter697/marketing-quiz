export class PriceStatistics {
  minPrice: number;
  maxPrice: number;
  statistics: { price: number, count: number }[];
}
