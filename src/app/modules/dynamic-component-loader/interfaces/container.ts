import { ViewContainerRef } from '@angular/core';

export interface Container {
  viewRef: ViewContainerRef;
}
