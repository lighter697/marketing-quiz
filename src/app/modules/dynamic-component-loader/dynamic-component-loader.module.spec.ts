import { DynamicComponentLoaderModule } from './dynamic-component-loader.module';

describe('DynamicComponentLoaderModule', () => {
  let dynamicComponentLoaderModule: DynamicComponentLoaderModule;

  beforeEach(() => {
    dynamicComponentLoaderModule = new DynamicComponentLoaderModule();
  });

  it('should create an instance', () => {
    expect(dynamicComponentLoaderModule).toBeTruthy();
  });
});
