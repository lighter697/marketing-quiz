import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ContentBuilderBricksModule} from './modules/content-builder-bricks/content-builder-bricks.module';
import {ContentBuilderCardsModule} from './modules/content-builder-cards/content-builder-cards.module';
import {ContentBuilderElementsModule} from './modules/content-builder-elements/content-builder-elements.module';
import {ContentBuilderPopupsModule} from './modules/content-builder-popups/content-builder-popups.module';

@NgModule({
  imports: [
    CommonModule,
    ContentBuilderBricksModule,
    ContentBuilderCardsModule,
    ContentBuilderElementsModule,
    ContentBuilderPopupsModule,
  ],
  declarations: []
})
export class ContentBuilderModule {
}
