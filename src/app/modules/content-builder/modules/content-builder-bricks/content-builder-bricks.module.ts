import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoverPageBrickComponent} from './components/cover-page-brick/cover-page-brick.component';
import {TextQuestionsBrickComponent} from './components/text-questions-brick/text-questions-brick.component';
import { ResultBuilderBrickComponent } from './components/result-builder-brick/result-builder-brick.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    CoverPageBrickComponent,
    TextQuestionsBrickComponent,
    ResultBuilderBrickComponent,
  ],
  exports: [
    CoverPageBrickComponent,
    TextQuestionsBrickComponent,
  ]
})
export class ContentBuilderBricksModule {
}
