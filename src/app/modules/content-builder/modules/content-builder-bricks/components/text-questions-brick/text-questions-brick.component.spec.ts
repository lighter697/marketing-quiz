import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextQuestionsBrickComponent } from './text-questions-brick.component';

describe('TextQuestionsBrickComponent', () => {
  let component: TextQuestionsBrickComponent;
  let fixture: ComponentFixture<TextQuestionsBrickComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextQuestionsBrickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextQuestionsBrickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
