import {Component, OnInit} from '@angular/core';
import {AbstractBrickComponent} from '../../../../../../core/abstract/components/abstract-brick/abstract-brick.component';

@Component({
  selector: 'app-text-questions-brick',
  templateUrl: './text-questions-brick.component.html',
  styleUrls: ['./text-questions-brick.component.scss']
})
export class TextQuestionsBrickComponent extends AbstractBrickComponent implements OnInit {
}
