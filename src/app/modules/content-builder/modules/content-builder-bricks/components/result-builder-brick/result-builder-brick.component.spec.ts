import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultBuilderBrickComponent } from './result-builder-brick.component';

describe('ResultBuilderBrickComponent', () => {
  let component: ResultBuilderBrickComponent;
  let fixture: ComponentFixture<ResultBuilderBrickComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultBuilderBrickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultBuilderBrickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
