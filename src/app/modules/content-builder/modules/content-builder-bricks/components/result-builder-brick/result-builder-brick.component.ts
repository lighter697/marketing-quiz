import {Component} from '@angular/core';
import {AbstractBrickComponent} from '../../../../../../core/abstract/components/abstract-brick/abstract-brick.component';

@Component({
  selector: 'app-result-builder-brick',
  templateUrl: './result-builder-brick.component.html',
  styleUrls: ['./result-builder-brick.component.scss']
})
export class ResultBuilderBrickComponent extends AbstractBrickComponent {
}
