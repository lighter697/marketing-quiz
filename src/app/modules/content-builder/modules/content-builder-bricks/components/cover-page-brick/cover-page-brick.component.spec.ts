import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoverPageBrickComponent } from './cover-page-brick.component';

describe('CoverPageBrickComponent', () => {
  let component: CoverPageBrickComponent;
  let fixture: ComponentFixture<CoverPageBrickComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoverPageBrickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoverPageBrickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
