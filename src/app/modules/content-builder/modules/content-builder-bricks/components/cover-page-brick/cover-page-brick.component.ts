import {Component, OnInit} from '@angular/core';
import {AbstractBrickComponent} from '../../../../../../core/abstract/components/abstract-brick/abstract-brick.component';
import {BrickDragActionService} from '../../../../../quiz/services/brick-drag-action/brick-drag-action.service';

@Component({
  selector: 'app-cover-page-brick',
  templateUrl: './cover-page-brick.component.html',
  styleUrls: ['./cover-page-brick.component.scss']
})
export class CoverPageBrickComponent extends AbstractBrickComponent implements OnInit {

  constructor(protected brickDragActionService: BrickDragActionService) {
    super(brickDragActionService);
  }


  ngOnInit() {
  }
}
