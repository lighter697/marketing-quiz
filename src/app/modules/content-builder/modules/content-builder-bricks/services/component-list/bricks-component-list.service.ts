import {Injectable} from '@angular/core';
import {AbstractDynamicList} from '../../../../../../core/abstract/services/abstract-dynamic-list';
import {CoverPageBrickComponent} from '../../components/cover-page-brick/cover-page-brick.component';
import {TextQuestionsBrickComponent} from '../../components/text-questions-brick/text-questions-brick.component';
import {ResultBuilderBrickComponent} from '../../components/result-builder-brick/result-builder-brick.component';

@Injectable()
export class BricksComponentListService extends AbstractDynamicList {

  protected list() {
    return {
      'cover-page': CoverPageBrickComponent,
      'text-questions': TextQuestionsBrickComponent,
      'result-builder': ResultBuilderBrickComponent,
    };
  }

}
