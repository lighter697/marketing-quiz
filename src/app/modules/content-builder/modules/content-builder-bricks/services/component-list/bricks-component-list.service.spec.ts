import { TestBed, inject } from '@angular/core/testing';

import { BricksComponentListService } from './bricks-component-list.service';

describe('BrickComponentListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BricksComponentListService]
    });
  });

  it('should be created', inject([BricksComponentListService], (service: BricksComponentListService) => {
    expect(service).toBeTruthy();
  }));
});
