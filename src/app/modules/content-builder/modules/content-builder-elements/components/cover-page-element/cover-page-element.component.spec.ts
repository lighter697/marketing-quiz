import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoverPageElementComponent } from './cover-page-element.component';

describe('CoverPageItemComponent', () => {
  let component: CoverPageElementComponent;
  let fixture: ComponentFixture<CoverPageElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoverPageElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoverPageElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
