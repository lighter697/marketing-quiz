import {Component} from '@angular/core';
import {AbstractElementComponent} from '../abstract-element/abstract-element.component';

@Component({
  selector: 'app-cover-page-element',
  templateUrl: './cover-page-element.component.html',
  styleUrls: ['./cover-page-element.component.scss']
})
export class CoverPageElementComponent extends AbstractElementComponent {
}
