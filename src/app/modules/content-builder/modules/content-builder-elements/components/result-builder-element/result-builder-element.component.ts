import {Component} from '@angular/core';
import {AbstractElementComponent} from '../abstract-element/abstract-element.component';

@Component({
  selector: 'app-result-builder-element',
  templateUrl: './result-builder-element.component.html',
  styleUrls: ['./result-builder-element.component.scss']
})
export class ResultBuilderElementComponent extends AbstractElementComponent {
}
