import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultBuilderElementComponent } from './result-builder-element.component';

describe('ResultBuilderElementComponent', () => {
  let component: ResultBuilderElementComponent;
  let fixture: ComponentFixture<ResultBuilderElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultBuilderElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultBuilderElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
