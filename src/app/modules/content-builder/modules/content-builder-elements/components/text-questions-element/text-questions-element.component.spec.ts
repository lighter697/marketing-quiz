import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextQuestionsElementComponent } from './text-questions-element.component';

describe('TextQuestionsItemComponent', () => {
  let component: TextQuestionsElementComponent;
  let fixture: ComponentFixture<TextQuestionsElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextQuestionsElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextQuestionsElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
