import {Component} from '@angular/core';
import {AbstractElementComponent} from '../abstract-element/abstract-element.component';

@Component({
  selector: 'app-text-questions-element',
  templateUrl: './text-questions-element.component.html',
  styleUrls: ['./text-questions-element.component.scss']
})
export class TextQuestionsElementComponent extends AbstractElementComponent {
}
