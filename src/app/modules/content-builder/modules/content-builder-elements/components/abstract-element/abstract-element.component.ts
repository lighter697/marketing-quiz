import {Injectable, Input, OnInit} from '@angular/core';
import {InputComponent} from '../../../../../dynamic-component-loader/models/input-component';
import {CardModel} from '../../../../../quiz/models/card.model';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PopupsComponentListService} from '../../../content-builder-popups/services/component-list/popups-component-list.service';
import {QuestionService} from '../../../../../quiz/services/question/question.service';
import {ActivatedRoute} from '@angular/router';
import {catchError, filter} from 'rxjs/operators';
import {from} from 'rxjs/observable/from';
import {of} from 'rxjs/observable/of';
import {plainToClass} from 'class-transformer';

@Injectable()
export abstract class AbstractElementComponent implements OnInit {
  @Input() inputComponent: InputComponent<CardModel>;

  constructor(
    private modal: NgbModal,
    private popupComponentListService: PopupsComponentListService,
    private questionService: QuestionService,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
  }

  edit() {
    const card = plainToClass(CardModel, this.inputComponent.params);
    this.openPopup(this.popupComponentListService.getComponentByAlias(this.inputComponent.component), {
      component: this.inputComponent.component,
      params: card
    })
      .pipe(
        filter(result => !!result),
      )
      .subscribe(result => {
        this.questionService.update(this.route.snapshot.params['id'], this.inputComponent.params.id, result).then();
      });
  }

  delete() {
    this.questionService.delete(this.route.snapshot.params['id'], this.inputComponent.params.id).then();
  }

  openPopup(component, inputComponent: InputComponent<CardModel>) {
    const modalRef = this.modal.open(component, {windowClass: 'modal-xxl'});
    modalRef.componentInstance.inputComponent = inputComponent;
    return from(modalRef.result).pipe(
      catchError(() => of(false))
    );
  }
}
