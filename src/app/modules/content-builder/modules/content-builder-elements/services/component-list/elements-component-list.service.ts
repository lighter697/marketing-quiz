import {Injectable} from '@angular/core';
import {CoverPageElementComponent} from '../../components/cover-page-element/cover-page-element.component';
import {TextQuestionsElementComponent} from '../../components/text-questions-element/text-questions-element.component';
import {AbstractDynamicList} from '../../../../../../core/abstract/services/abstract-dynamic-list';
import {ResultBuilderElementComponent} from '../../components/result-builder-element/result-builder-element.component';

@Injectable()
export class ElementsComponentListService extends AbstractDynamicList {

  protected list() {
    return {
      'cover-page': CoverPageElementComponent,
      'text-questions': TextQuestionsElementComponent,
      'result-builder': ResultBuilderElementComponent,
    };
  }

}
