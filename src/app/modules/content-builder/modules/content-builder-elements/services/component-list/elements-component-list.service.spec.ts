import { TestBed, inject } from '@angular/core/testing';

import { ElementsComponentListService } from './elements-component-list.service';

describe('ItemComponentListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ElementsComponentListService]
    });
  });

  it('should be created', inject([ElementsComponentListService], (service: ElementsComponentListService) => {
    expect(service).toBeTruthy();
  }));
});
