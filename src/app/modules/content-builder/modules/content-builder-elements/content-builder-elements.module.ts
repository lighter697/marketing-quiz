import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CoverPageElementComponent} from './components/cover-page-element/cover-page-element.component';
import {TextQuestionsElementComponent} from './components/text-questions-element/text-questions-element.component';
import { ResultBuilderElementComponent } from './components/result-builder-element/result-builder-element.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    CoverPageElementComponent,
    TextQuestionsElementComponent,
    ResultBuilderElementComponent,
  ],
  exports: [
    CoverPageElementComponent,
    TextQuestionsElementComponent,
  ]
})
export class ContentBuilderElementsModule { }
