import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultBuilderCardComponent } from './result-builder-card.component';

describe('ResultBuilderCardComponent', () => {
  let component: ResultBuilderCardComponent;
  let fixture: ComponentFixture<ResultBuilderCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultBuilderCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultBuilderCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
