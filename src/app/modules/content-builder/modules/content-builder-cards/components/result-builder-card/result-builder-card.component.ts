import {Component} from '@angular/core';
import {AbstractCardComponent} from '../abstract-card/abstract-card.component';

@Component({
  selector: 'app-result-builder-card',
  templateUrl: './result-builder-card.component.html',
  styleUrls: ['./result-builder-card.component.scss']
})
export class ResultBuilderCardComponent extends AbstractCardComponent {
}
