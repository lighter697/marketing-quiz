import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoverPageCardComponent } from './cover-page-card.component';

describe('CoverPagePreviewComponent', () => {
  let component: CoverPageCardComponent;
  let fixture: ComponentFixture<CoverPageCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoverPageCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoverPageCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
