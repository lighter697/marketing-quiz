import {Component} from '@angular/core';
import {AbstractCardComponent} from '../abstract-card/abstract-card.component';

@Component({
  selector: 'app-cover-page-preview',
  templateUrl: './cover-page-card.component.html',
  styleUrls: ['./cover-page-card.component.scss'],
})
export class CoverPageCardComponent extends AbstractCardComponent {
}
