import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextQuestionsCardComponent } from './text-questions-card.component';

describe('TextQuestionsPreviewComponent', () => {
  let component: TextQuestionsCardComponent;
  let fixture: ComponentFixture<TextQuestionsCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextQuestionsCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextQuestionsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
