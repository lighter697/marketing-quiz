import { Component, OnInit } from '@angular/core';
import {AbstractCardComponent} from '../abstract-card/abstract-card.component';

@Component({
  selector: 'app-text-questions-preview',
  templateUrl: './text-questions-card.component.html',
  styleUrls: ['./text-questions-card.component.scss']
})
export class TextQuestionsCardComponent extends AbstractCardComponent {
}
