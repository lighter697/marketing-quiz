import {Injectable, Input, OnInit} from '@angular/core';
import {InputComponent} from '../../../../../dynamic-component-loader/models/input-component';
import {CardModel} from '../../../../../quiz/models/card.model';

@Injectable()
export abstract class AbstractCardComponent implements OnInit {
  @Input() inputComponent: InputComponent<CardModel>;

  constructor() {
  }

  ngOnInit() {
  }
}
