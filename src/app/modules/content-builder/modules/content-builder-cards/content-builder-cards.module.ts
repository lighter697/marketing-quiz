import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoverPageCardComponent} from './components/cover-page-card/cover-page-card.component';
import {TextQuestionsCardComponent} from './components/text-questions-card/text-questions-card.component';
import { ResultBuilderCardComponent } from './components/result-builder-card/result-builder-card.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    CoverPageCardComponent,
    TextQuestionsCardComponent,
    ResultBuilderCardComponent,
  ],
  exports: [
    CoverPageCardComponent,
    TextQuestionsCardComponent,
  ]
})
export class ContentBuilderCardsModule {
}
