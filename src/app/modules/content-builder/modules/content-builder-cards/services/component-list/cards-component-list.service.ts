import {Injectable} from '@angular/core';
import {AbstractDynamicList} from '../../../../../../core/abstract/services/abstract-dynamic-list';
import {CoverPageCardComponent} from '../../components/cover-page-card/cover-page-card.component';
import {TextQuestionsCardComponent} from '../../components/text-questions-card/text-questions-card.component';
import {ResultBuilderCardComponent} from '../../components/result-builder-card/result-builder-card.component';

@Injectable()
export class CardsComponentListService extends AbstractDynamicList {
  protected list() {
    return {
      'cover-page': CoverPageCardComponent,
      'text-questions': TextQuestionsCardComponent,
      'result-builder': ResultBuilderCardComponent,
    };
  }
}
