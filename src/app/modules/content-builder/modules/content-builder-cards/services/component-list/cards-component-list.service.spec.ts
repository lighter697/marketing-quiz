import { TestBed, inject } from '@angular/core/testing';

import { CardsComponentListService } from './cards-component-list.service';

describe('PreviewComponentListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CardsComponentListService]
    });
  });

  it('should be created', inject([CardsComponentListService], (service: CardsComponentListService) => {
    expect(service).toBeTruthy();
  }));
});
