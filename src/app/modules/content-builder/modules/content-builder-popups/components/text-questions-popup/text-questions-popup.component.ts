import {Component, OnInit} from '@angular/core';
import {AbstractPopupComponent} from '../abstract-popup/abstract-popup.component';
import {ComponentListService} from '../../../../../../core/services/component-list/component-list.service';
import {CardsComponentListService} from '../../../content-builder-cards/services/component-list/cards-component-list.service';
import {map, startWith} from 'rxjs/operators';
import {FormArray, Validators} from '@angular/forms';

@Component({
  selector: 'app-text-questions-popup',
  templateUrl: './text-questions-popup.component.html',
  styleUrls: ['./text-questions-popup.component.scss'],
  providers: [
    {provide: ComponentListService, useClass: CardsComponentListService}
  ]
})
export class TextQuestionsPopupComponent extends AbstractPopupComponent implements OnInit {
  ngOnInit() {
    this.group = this.fb.group({
      title: this.fb.control(this.inputComponent.params.title, [Validators.required, Validators.min(3)]),
      image: this.fb.control(this.inputComponent.params.image),
      answers: this.fb.array(this.inputComponent.params.answers.length ? this.inputComponent.params.answers.map(answer => {
        return this.fb.group({
          answer: this.fb.control(answer.answer, [Validators.required, Validators.min(3)])
        });
      }) : [
        this.fb.group({
          answer: this.fb.control(null, [Validators.required, Validators.min(3)])
        })
      ]),
    });

    this.combined$ = this.group.valueChanges.pipe(
      startWith(this.group.value),
      map((form) => {
        return {
          component: this.inputComponent.component,
          params: {...this.inputComponent.params, ...{...form, answers: form['answers'].filter(answer => !!answer.answer)}}
        };
      }),
    );
  }

  addAnswer() {
    this.answers.push(this.fb.group({
      answer: this.fb.control(null, [Validators.required, Validators.min(3)])
    }));
  }

  removeAnswer(index) {
    if (this.answers.controls.length > 1) {
      this.answers.removeAt(index);
    }
  }

  get answers() {
    return this.group.get('answers') as FormArray;
  }
}
