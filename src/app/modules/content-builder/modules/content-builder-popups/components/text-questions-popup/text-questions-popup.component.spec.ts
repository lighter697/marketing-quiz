import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextQuestionsPopupComponent } from './text-questions-popup.component';

describe('TextQuestionsPopupComponent', () => {
  let component: TextQuestionsPopupComponent;
  let fixture: ComponentFixture<TextQuestionsPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextQuestionsPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextQuestionsPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
