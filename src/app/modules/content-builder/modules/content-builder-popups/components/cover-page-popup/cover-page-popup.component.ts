import {Component, OnInit} from '@angular/core';
import {AbstractPopupComponent} from '../abstract-popup/abstract-popup.component';
import {ComponentListService} from '../../../../../../core/services/component-list/component-list.service';
import {CardsComponentListService} from '../../../content-builder-cards/services/component-list/cards-component-list.service';

@Component({
  selector: 'app-cover-page-popup',
  templateUrl: './cover-page-popup.component.html',
  styleUrls: ['./cover-page-popup.component.scss'],
  providers: [
    {provide: ComponentListService, useClass: CardsComponentListService}
  ]
})
export class CoverPagePopupComponent extends AbstractPopupComponent implements OnInit {

}
