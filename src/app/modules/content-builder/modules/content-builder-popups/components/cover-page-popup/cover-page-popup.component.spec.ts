import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoverPagePopupComponent } from './cover-page-popup.component';

describe('CoverPagePopupComponent', () => {
  let component: CoverPagePopupComponent;
  let fixture: ComponentFixture<CoverPagePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoverPagePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoverPagePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
