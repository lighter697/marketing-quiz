import {Injectable, Input, OnInit} from '@angular/core';
import {InputComponent} from '../../../../../dynamic-component-loader/models/input-component';
import {CardModel} from '../../../../../quiz/models/card.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {map, startWith} from 'rxjs/operators';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export abstract class AbstractPopupComponent implements OnInit {
  @Input() inputComponent: InputComponent<CardModel>;
  group: FormGroup;
  combined$: Observable<InputComponent<CardModel>>;

  constructor(
    protected fb: FormBuilder,
    protected modal: NgbActiveModal,
  ) {
  }

  ngOnInit() {
    this.initForm();
    this.initCombined();
  }

  initCombined() {
    this.combined$ = this.group.valueChanges.pipe(
      startWith(this.group.value),
      map((form) => {
        return {component: this.inputComponent.component, params: {...this.inputComponent.params, ...form}};
      }),
    );
  }

  initForm() {
    this.group = this.fb.group({
      title: this.fb.control(this.inputComponent.params.title, [Validators.required]),
      description: this.fb.control(this.inputComponent.params.description),
      callToAction: this.fb.control(this.inputComponent.params.callToAction, [Validators.required]),
      image: this.fb.control(this.inputComponent.params.image),
    });
  }

  imageChanged(file) {
    if (file) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.group.get('image').patchValue(reader.result);
      };
    } else {
      this.group.get('image').patchValue(null);
    }
  }

  dismiss() {
    this.modal.dismiss();
  }

  close() {
    if (this.group.valid) {
      this.modal.close({...this.inputComponent.params, component: this.inputComponent.component, ...this.group.value});
    } else {
      this.markFormGroupTouched(this.group);
    }
  }

  private markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();
      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }
}
