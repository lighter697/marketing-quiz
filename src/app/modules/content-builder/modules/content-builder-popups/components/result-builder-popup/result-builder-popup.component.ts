import {Component, OnInit} from '@angular/core';
import {AbstractPopupComponent} from '../abstract-popup/abstract-popup.component';
import {ComponentListService} from '../../../../../../core/services/component-list/component-list.service';
import {CardsComponentListService} from '../../../content-builder-cards/services/component-list/cards-component-list.service';
import {Validators} from '@angular/forms';

@Component({
  selector: 'app-result-builder-popup',
  templateUrl: './result-builder-popup.component.html',
  styleUrls: ['./result-builder-popup.component.scss'],
  providers: [
    {provide: ComponentListService, useClass: CardsComponentListService}
  ]
})
export class ResultBuilderPopupComponent extends AbstractPopupComponent implements OnInit {

  ngOnInit() {
    this.group = this.fb.group({
      title: this.fb.control(this.inputComponent.params.title, [Validators.required]),
      description: this.fb.control(this.inputComponent.params.description),
      callToAction: this.fb.control(this.inputComponent.params.callToAction, [Validators.required]),
    });

    this.initCombined();
  }
}
