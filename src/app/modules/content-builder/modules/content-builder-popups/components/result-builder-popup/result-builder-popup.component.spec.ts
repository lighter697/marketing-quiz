import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultBuilderPopupComponent } from './result-builder-popup.component';

describe('ResultBuilderPopupComponent', () => {
  let component: ResultBuilderPopupComponent;
  let fixture: ComponentFixture<ResultBuilderPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultBuilderPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultBuilderPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
