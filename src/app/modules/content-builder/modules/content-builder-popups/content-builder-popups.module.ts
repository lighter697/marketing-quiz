import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoverPagePopupComponent} from './components/cover-page-popup/cover-page-popup.component';
import {TextQuestionsPopupComponent} from './components/text-questions-popup/text-questions-popup.component';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../../../shared/shared.module';
import {DynamicComponentLoaderModule} from '../../../dynamic-component-loader';
import {TextQuestionsCardComponent} from '../content-builder-cards/components/text-questions-card/text-questions-card.component';
import {CoverPageCardComponent} from '../content-builder-cards/components/cover-page-card/cover-page-card.component';
import { ResultBuilderPopupComponent } from './components/result-builder-popup/result-builder-popup.component';
import {ResultBuilderCardComponent} from '../content-builder-cards/components/result-builder-card/result-builder-card.component';

@NgModule({
  entryComponents: [
    // Cards
    CoverPageCardComponent,
    TextQuestionsCardComponent,
    ResultBuilderCardComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    DynamicComponentLoaderModule
  ],
  declarations: [
    CoverPagePopupComponent,
    TextQuestionsPopupComponent,
    ResultBuilderPopupComponent,
  ],
  exports: [
    CoverPagePopupComponent,
    TextQuestionsPopupComponent,
  ],
})
export class ContentBuilderPopupsModule {
}
