import { TestBed, inject } from '@angular/core/testing';

import { PopupsComponentListService } from './popups-component-list.service';

describe('PopupComponentListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PopupsComponentListService]
    });
  });

  it('should be created', inject([PopupsComponentListService], (service: PopupsComponentListService) => {
    expect(service).toBeTruthy();
  }));
});
