import {Injectable} from '@angular/core';
import {AbstractDynamicList} from '../../../../../../core/abstract/services/abstract-dynamic-list';
import {CoverPagePopupComponent} from '../../components/cover-page-popup/cover-page-popup.component';
import {TextQuestionsPopupComponent} from '../../components/text-questions-popup/text-questions-popup.component';
import {ResultBuilderPopupComponent} from '../../components/result-builder-popup/result-builder-popup.component';

@Injectable()
export class PopupsComponentListService extends AbstractDynamicList {
  protected list() {
    return {
      'cover-page': CoverPagePopupComponent,
      'text-questions': TextQuestionsPopupComponent,
      'result-builder': ResultBuilderPopupComponent,
    };
  }
}
