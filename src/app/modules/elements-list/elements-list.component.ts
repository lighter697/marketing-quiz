import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {combineLatest} from 'rxjs/observable/combineLatest';
import {catchError, map, switchMap, take, withLatestFrom} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {Subject} from 'rxjs/Subject';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {from} from 'rxjs/observable/from';
import {SectionModel} from '../quiz/models/section.model';
import {InputComponent} from '../dynamic-component-loader/models/input-component';
import {CardModel} from '../quiz/models/card.model';
import {QuestionService} from '../quiz/services/question/question.service';
import {BrickDragActionService} from '../quiz/services/brick-drag-action/brick-drag-action.service';
import {PopupsComponentListService} from '../content-builder/modules/content-builder-popups/services/component-list/popups-component-list.service';
import {plainToClass} from 'class-transformer';

@Component({
  selector: 'app-elements-list',
  templateUrl: './elements-list.component.html',
  styleUrls: ['./elements-list.component.scss'],
})
export class ElementsListComponent implements OnInit, OnDestroy {

  subscription = new Subscription();
  drop$ = new Subject();

  sections$: Observable<SectionModel[]> = of([
    {
      id: 1,
      title: 'Start Screen',
      description: 'Drag & Drop start screen elements here.',
      className: 'jumbotron-success',
      maxElements: 1
    },
    {
      id: 2,
      title: 'Custom Elements',
      description: 'Drag & Drop content elements here.',
      className: 'jumbotron-primary',
      maxElements: null
    },
    {
      id: 3,
      title: 'Results',
      description: 'Drag & Drop results elements here.',
      className: 'jumbotron-secondary',
      maxElements: 1
    }
  ]);

  components$: Observable<InputComponent<CardModel>[]> = this.route.params.pipe(
    switchMap(params => this.questionService.all(params['id']))
  );

  combined$: Observable<SectionModel[]> = combineLatest(this.sections$, this.components$).pipe(
    map(([sections, components, bricks]) => {
      return sections.map(section => {
        return {
          ...section,
          components: components.length ? components.filter(
            (component: CardModel) => parseInt(component.sectionId, 10) === section.id
          ).map(component => {
            return {
              component: component.component,
              params: {...component, id: component.$key}
            };
          }) : []
        };
      });
    }),
  );

  dragOver$ = this.brickDragActionService.drag().pipe(map((drag: InputComponent<CardModel>) => drag ? drag.params.sectionId : null));

  constructor(
    private questionService: QuestionService,
    private route: ActivatedRoute,
    private brickDragActionService: BrickDragActionService,
    private popupComponentListService: PopupsComponentListService,
    private modal: NgbModal,
  ) {
  }

  ngOnInit() {
    this.subscription.add(
      this.drop$.pipe(
        withLatestFrom(this.brickDragActionService.drag(), this.route.params),
        switchMap(([drop, drag, params]) => {
          const card = plainToClass(CardModel, drag.params);
          return this.openPopup(this.popupComponentListService.getComponentByAlias(drag.component), {
            component: drag.component,
            params: card
          });
        }),
      ).subscribe((result) => {
        this.questionService.create(this.route.snapshot.params['id'], result).then();
        this.brickDragActionService.endDrag();
      })
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  drop(event, section) {
    event.preventDefault();
    this.drop$.next({...event, section: section});
  }

  allowDrop(event, section: SectionModel, components: InputComponent<CardModel>[]) {
    this.brickDragActionService.drag().pipe(
      take(1),
    ).subscribe((drag: InputComponent<CardModel>) => {
      if (drag.params.sectionId === section.id && (section.maxElements === null || section.maxElements > components.length)) {
        event.preventDefault();
      }
    });
  }

  openPopup(component, inputComponent: InputComponent<CardModel>) {
    const modalRef = this.modal.open(component, {windowClass: 'modal-xxl'});
    modalRef.componentInstance.inputComponent = inputComponent;
    return from(modalRef.result).pipe(
      catchError(error => of(false))
    );
  }
}
