import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ComponentListService} from '../../core/services/component-list/component-list.service';
import {ElementsComponentListService} from '../content-builder/modules/content-builder-elements/services/component-list/elements-component-list.service';
import {ElementsListComponent} from './elements-list.component';
import {DynamicComponentLoaderModule} from '../dynamic-component-loader';
import {ContentBuilderElementsModule} from '../content-builder/modules/content-builder-elements/content-builder-elements.module';
import {CoverPageElementComponent} from '../content-builder/modules/content-builder-elements/components/cover-page-element/cover-page-element.component';
import {TextQuestionsElementComponent} from '../content-builder/modules/content-builder-elements/components/text-questions-element/text-questions-element.component';
import {ContentBuilderPopupsModule} from '../content-builder/modules/content-builder-popups/content-builder-popups.module';
import {TextQuestionsPopupComponent} from '../content-builder/modules/content-builder-popups/components/text-questions-popup/text-questions-popup.component';
import {CoverPagePopupComponent} from '../content-builder/modules/content-builder-popups/components/cover-page-popup/cover-page-popup.component';
import {ContentBuilderCardsModule} from '../content-builder/modules/content-builder-cards/content-builder-cards.module';
import {ResultBuilderPopupComponent} from '../content-builder/modules/content-builder-popups/components/result-builder-popup/result-builder-popup.component';
import {ResultBuilderElementComponent} from '../content-builder/modules/content-builder-elements/components/result-builder-element/result-builder-element.component';

@NgModule({
  entryComponents: [
    // Elements
    CoverPageElementComponent,
    TextQuestionsElementComponent,
    ResultBuilderElementComponent,
    // Popups
    CoverPagePopupComponent,
    TextQuestionsPopupComponent,
    ResultBuilderPopupComponent,
  ],
  imports: [
    CommonModule,
    DynamicComponentLoaderModule,
    // Content builder modules
    ContentBuilderElementsModule,
    ContentBuilderPopupsModule,
    ContentBuilderCardsModule,
  ],
  declarations: [
    ElementsListComponent
  ],
  exports: [
    ElementsListComponent
  ],
  providers: [
    {provide: ComponentListService, useClass: ElementsComponentListService}
  ]
})
export class ElementsListModule {
}
