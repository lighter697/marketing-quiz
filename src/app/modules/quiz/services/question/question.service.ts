import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';

@Injectable()
export class QuestionService {

  constructor(
    private db: AngularFireDatabase,
  ) {
  }

  all(quizId) {
    return this.db.list('/quizzes/' + quizId + '/cards');
  }

  create(quizId, data) {
    return this.db.list('/quizzes/' + quizId + '/cards').push(data);
  }

  update(quizId, cardId, data) {
    return this.db.object('/quizzes/' + quizId + '/cards/' + cardId).set(data);
  }

  delete(quizId, cardId) {
    return this.db.object('/quizzes/' + quizId + '/cards/' + cardId).remove();
  }
}
