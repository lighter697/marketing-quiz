import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {Observable} from 'rxjs/Observable';
import {QuizModel} from '../../models/quiz.model';

@Injectable()
export class QuizService {

  constructor(
    private db: AngularFireDatabase,
  ) {
  }

  all(): Observable<QuizModel[]> {
    return this.db.list('/quizzes');
  }

  get(key) {
    return this.db.object('/quizzes/' + key);
  }

  setPublished(key, published) {
    return this.db.object('/quizzes/' + key).update({
      published: published
    });
  }

  create(data) {
    return this.db.list('/quizzes').push(data);
  }

  update(key, data) {
    return this.db.object('/quizzes/' + key).update(data);
  }

  delete(key) {
    return this.db.object('/quizzes/' + key).remove();
  }
}
