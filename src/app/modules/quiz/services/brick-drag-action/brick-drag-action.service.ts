import {Injectable} from '@angular/core';
import {filter} from 'rxjs/operators';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class BrickDragActionService {

  private data: BehaviorSubject<any> = new BehaviorSubject(null);

  startDrag(data) {
    this.data.next(data);
  }

  endDrag() {
    this.data.next(null);
  }

  drag(): Observable<any> {
    return this.data;
  }

}
