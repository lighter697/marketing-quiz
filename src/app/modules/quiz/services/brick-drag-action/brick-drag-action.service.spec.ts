import { TestBed, inject } from '@angular/core/testing';

import { BrickDragActionService } from './brick-drag-action.service';

describe('BrickDragActionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BrickDragActionService]
    });
  });

  it('should be created', inject([BrickDragActionService], (service: BrickDragActionService) => {
    expect(service).toBeTruthy();
  }));
});
