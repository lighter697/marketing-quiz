import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {QuizComponent} from './quiz.component';
import {QuizIndexComponent} from './pages/quiz-index/quiz-index.component';
import {QuizEditComponent} from './pages/quiz-edit/quiz-edit.component';
import {EditQuizGuard} from '../../core/guards/edit-quiz.guard';

const routes: Routes = [
  {
    path: '', component: QuizComponent, children: [
      {path: '', component: QuizIndexComponent},
      {path: ':id/edit', component: QuizEditComponent, canActivate: [EditQuizGuard]},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [EditQuizGuard]
})
export class QuizRoutingModule {
}
