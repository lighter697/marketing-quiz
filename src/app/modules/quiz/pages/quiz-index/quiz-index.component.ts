import {Component, OnInit} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {QuizModel} from '../../models/quiz.model';
import {map, switchMap, tap} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import {QuizService} from '../../services/quiz/quiz.service';
import {QuestionService} from '../../services/question/question.service';
import {combineLatest} from 'rxjs/observable/combineLatest';
import {of} from 'rxjs/observable/of';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-quiz-index',
  templateUrl: './quiz-index.component.html',
  styleUrls: ['./quiz-index.component.scss']
})
export class QuizIndexComponent implements OnInit {

  quizzes$: Observable<QuizModel[]>;
  showSpinner = false;

  constructor(
    private db: AngularFireDatabase,
    private quizService: QuizService,
    private questionService: QuestionService,
  ) {
  }

  ngOnInit() {
    this.showSpinner = true;
    this.quizzes$ = this.quizService.all().pipe(
      map((quizzes: QuizModel[]) => quizzes.map(quiz => {
        return {...quiz, id: quiz.$key};
      })),
      switchMap((quizzes: QuizModel[]) =>
        combineLatest(
          of(quizzes),
          quizzes.length ? combineLatest(quizzes.map(quiz => this.questionService.all(quiz.id))) : of([])
        )
      ),
      map(([quizzes, questions]) => {
        return quizzes.map((quiz, index) => {
          if (questions[index].length) {
            const component = questions[index].find(question => question.component === environment.coverPageComponentName);
            if (component) {
              if (component.image) {
                return {...quiz, image: component.image};
              }
            }
          }
          return quiz;
        });
      }),
      tap(() => this.showSpinner = false)
    );
  }

}
