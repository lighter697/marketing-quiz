import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {QuizRoutingModule} from './quiz-routing.module';
import {CardComponent} from './components/card/card.component';
import {SharedModule} from '../../shared/shared.module';
import {QuizComponent} from './quiz.component';
import {ReactiveFormsModule} from '@angular/forms';
import {QuizService} from './services/quiz/quiz.service';
import {ModalConfirmComponent} from '../../shared/components/modal-confirm/modal-confirm.component';
import {QuestionService} from './services/question/question.service';
import {DynamicComponentLoaderModule} from '../dynamic-component-loader';
import {QuizIndexComponent} from './pages/quiz-index/quiz-index.component';
import {QuizEditComponent} from './pages/quiz-edit/quiz-edit.component';
import {BrickDragActionService} from './services/brick-drag-action/brick-drag-action.service';
import {PopupsComponentListService} from '../content-builder/modules/content-builder-popups/services/component-list/popups-component-list.service';
import {BricksListModule} from '../bricks-list/bricks-list.module';
import {ElementsListModule} from '../elements-list/elements-list.module';
import {QuizFormComponent} from './components/quiz-form/quiz-form.component';
import {QuizTitleComponent} from './components/quiz-title/quiz-title.component';
import {ModalDeleteComponent} from '../../shared/components/modal-delete/modal-delete.component';

@NgModule({
  entryComponents: [
    ModalConfirmComponent,
    ModalDeleteComponent,
  ],
  imports: [
    CommonModule,
    QuizRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    DynamicComponentLoaderModule,
    BricksListModule,
    ElementsListModule,
  ],
  declarations: [
    QuizComponent,
    QuizIndexComponent,
    CardComponent,
    QuizEditComponent,
    QuizFormComponent,
    QuizTitleComponent,
  ],
  providers: [QuizService, QuestionService, BrickDragActionService, PopupsComponentListService]
})
export class QuizModule {
}
