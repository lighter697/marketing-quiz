import {InputComponent} from '../../dynamic-component-loader/models/input-component';

export class SectionModel {
  id: any;
  title: string;
  description: string;
  className: string;
  components: InputComponent<any>[];
  maxElements: number;
}
