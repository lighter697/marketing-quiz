export class CardModel {
  id: any;
  title: string;
  sectionId: any;
  component: any;
  name: string;
  description: string;
  callToAction: string;
  answers: { answer: string }[] = [];
  image?: string;
}
