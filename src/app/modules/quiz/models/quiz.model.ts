export class QuizModel {
  id: any;
  name: string;
  published: boolean;
  image: string;
  $key?: string;
}
