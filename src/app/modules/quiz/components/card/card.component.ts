import {Component, Input, OnInit} from '@angular/core';
import {QuizModel} from '../../models/quiz.model';
import {QuizService} from '../../services/quiz/quiz.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ModalConfirmComponent} from '../../../../shared/components/modal-confirm/modal-confirm.component';
import {from} from 'rxjs/observable/from';
import {catchError, filter} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {ModalDeleteComponent} from '../../../../shared/components/modal-delete/modal-delete.component';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() quiz: QuizModel;

  constructor(
    private quizService: QuizService,
    private modal: NgbModal
  ) {
  }

  ngOnInit() {
  }

  publish(status) {
    this.createModal(
      'Notice!',
      'Are you sure that you want to <b>' + (status ? 'publish' : 'unpublish') + '</b> quiz "' + this.quiz.name + '"?'
    ).subscribe(() => {
      this.quizService.setPublished(this.quiz.id, status);
    });
  }

  delete() {
    const modalRef = this.modal.open(ModalDeleteComponent);
    modalRef.componentInstance.title = 'Notice!';
    modalRef.componentInstance.name = this.quiz.name;
    modalRef.componentInstance.text = 'Are you sure that you want to <b>delete</b> quiz "' + this.quiz.name +
      '"? Type the quiz name below to delete.';
    from(modalRef.result).pipe(
      catchError(() => of(false)),
      filter(result => !!result)
    ).subscribe(() => {
      this.quizService.delete(this.quiz.id);
    });
  }

  private createModal(title, text) {
    const modalRef = this.modal.open(ModalConfirmComponent);
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.text = text;
    return from(modalRef.result).pipe(
      catchError(() => of(false)),
      filter(result => !!result)
    );
  }
}
