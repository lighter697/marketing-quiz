import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {QuizService} from '../../services/quiz/quiz.service';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {debounceTime, distinctUntilChanged, filter, switchMap, tap} from 'rxjs/operators';
import {from} from 'rxjs/observable/from';

@Component({
  selector: 'app-quiz-title',
  templateUrl: './quiz-title.component.html',
  styleUrls: ['./quiz-title.component.scss']
})
export class QuizTitleComponent implements OnInit, OnDestroy {

  group: FormGroup;
  subscription = new Subscription();
  formLoading = false;
  formPopulated = false;

  constructor(
    private fb: FormBuilder,
    private quizService: QuizService,
    private route: ActivatedRoute
  ) {
    this.group = this.fb.group({
      name: this.fb.control(null, [Validators.required])
    });
  }

  ngOnInit() {
    this.subscription.add(
      this.group.valueChanges.pipe(
        distinctUntilChanged(),
        filter(() => this.formPopulated),
        tap(() => this.formLoading = true),
        debounceTime(1000),
        switchMap(form => from(this.quizService.update(this.route.snapshot.params['id'], form))),
      ).subscribe(form => {
        this.formLoading = false;
      })
    );
    this.subscription.add(
      this.route.params.pipe(
        switchMap(params => this.quizService.get(params['id']))
      ).subscribe(result => {
        this.group.patchValue(result);
        this.formPopulated = true;
      })
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
