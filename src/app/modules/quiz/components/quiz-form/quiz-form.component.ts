import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-quiz-form',
  templateUrl: './quiz-form.component.html',
  styleUrls: ['./quiz-form.component.scss']
})
export class QuizFormComponent implements OnInit {

  @Input() group: FormGroup;
  @Output() save: EventEmitter<any> = new EventEmitter<any>();
  @Input() inProgress = false;
  @Input() questionsLink = null;

  constructor() {
  }

  ngOnInit() {
  }

  saveForm() {
    this.save.emit(this.group.value);
  }


  imageChanged(file) {
    if (file) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.group.get('image').patchValue(reader.result);
      };
    } else {
      this.group.get('image').patchValue(null);
    }
  }

}
