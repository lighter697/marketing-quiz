import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BricksListComponent} from './bricks-list.component';
import {DynamicComponentLoaderModule} from '../dynamic-component-loader';
import {CoverPageBrickComponent} from '../content-builder/modules/content-builder-bricks/components/cover-page-brick/cover-page-brick.component';
import {ContentBuilderBricksModule} from '../content-builder/modules/content-builder-bricks/content-builder-bricks.module';
import {TextQuestionsBrickComponent} from '../content-builder/modules/content-builder-bricks/components/text-questions-brick/text-questions-brick.component';
import {ResultBuilderBrickComponent} from '../content-builder/modules/content-builder-bricks/components/result-builder-brick/result-builder-brick.component';

@NgModule({
  entryComponents: [
    CoverPageBrickComponent,
    TextQuestionsBrickComponent,
    ResultBuilderBrickComponent,
  ],
  imports: [
    CommonModule,
    DynamicComponentLoaderModule,
    ContentBuilderBricksModule,
  ],
  declarations: [
    BricksListComponent
  ],
  exports: [
    BricksListComponent,
  ]
})
export class BricksListModule {
}
