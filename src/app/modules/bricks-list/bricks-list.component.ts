import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {combineLatest} from 'rxjs/observable/combineLatest';
import {SectionModel} from '../quiz/models/section.model';
import {map} from 'rxjs/operators';
import {InputComponent} from '../dynamic-component-loader/models/input-component';
import {CardModel} from '../quiz/models/card.model';
import {of} from 'rxjs/observable/of';
import {ComponentListService} from '../../core/services/component-list/component-list.service';
import {BricksComponentListService} from '../content-builder/modules/content-builder-bricks/services/component-list/bricks-component-list.service';
import {BrickModel} from '../quiz/models/brick.model';

@Component({
  selector: 'app-bricks-list',
  templateUrl: './bricks-list.component.html',
  styleUrls: ['./bricks-list.component.scss'],
  providers: [
    {provide: ComponentListService, useClass: BricksComponentListService}
  ]
})
export class BricksListComponent implements OnInit {

  sections$: Observable<SectionModel[]> = of([
    {
      id: 1,
      title: 'Start Screen',
      className: 'jumbotron-success',
    },
    {
      id: 2,
      title: 'Custom Elements',
      className: 'jumbotron-primary',
    },
    {
      id: 3,
      title: 'Results',
      className: 'jumbotron-danger',
    }
  ]);

  bricks$: Observable<InputComponent<BrickModel>[]> = of([
    {
      component: 'cover-page',
      params: {
        sectionId: 1,
        component: 'cover-page',
        title: 'Cover Page',
      },
    },
    {
      component: 'text-questions',
      params: {
        sectionId: 2,
        component: 'text-questions',
        title: 'Text Questions',
      },
    },
    {
      component: 'result-builder',
      params: {
        sectionId: 3,
        component: 'result-builder',
        title: 'Result Builder',
      },
    }
  ]);

  combined$: Observable<SectionModel[]> = combineLatest(this.sections$, this.bricks$).pipe(
    map(([sections, components]) => {
      return sections.map(section => {
        return {...section, components: components.filter(component => component.params.sectionId === section.id)};
      });
    }),
  );

  constructor() {
  }

  ngOnInit() {
  }

}
