import {Directive, ElementRef, EventEmitter, HostBinding, HostListener, Input, Output} from '@angular/core';

@Directive({
  selector: '[appDnd]'
})
export class DndDirective {

  @Input() allowedExtensions: Array<string> = [];
  @Output() filesChangeEmiter: EventEmitter<File[]> = new EventEmitter();
  @Output() filesInvalidEmiter: EventEmitter<File[]> = new EventEmitter();
  @HostBinding('class.dragover') private dragover = false;

  constructor(private elementRef: ElementRef) {
  }

  @HostListener('dragover', ['$event'])
  onDragOver(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.dragover = true;
  }

  @HostListener('dragleave', ['$event'])
  onDragLeave(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.dragover = false;
  }

  @HostListener('drop', ['$event'])
  onDrop(event) {
    event.preventDefault();
    event.stopPropagation();
    this.dragover = false;
    const files = event.dataTransfer.files;
    const validFiles: Array<File> = [];
    if (files.length > 0) {
      Object.values(files).forEach((file) => {
        const ext = file.name.split('.')[file.name.split('.').length - 1];
        if (this.allowedExtensions.lastIndexOf(ext) !== -1) {
          validFiles.push(file);
        }
      });
      this.filesChangeEmiter.emit(validFiles);
    }
  }
}
