import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';

@Component({
  selector: 'app-modal-delete',
  templateUrl: './modal-delete.component.html',
  styleUrls: ['./modal-delete.component.scss']
})
export class ModalDeleteComponent implements OnInit {

  @Input() text: string;
  @Input() name: string;

  @Input() title = 'Notice!';
  @Input() cancel = 'Cancel';
  @Input() confirm = 'Yes';

  group: FormGroup;

  constructor(
    public modal: NgbActiveModal,
    private fb: FormBuilder,
  ) {
  }

  ngOnInit() {
    this.group = this.fb.group({
      name: this.fb.control(null, [this.equalsTo()]),
    });
  }

  private equalsTo(): ValidatorFn {
    return (input: AbstractControl): ValidationErrors => {
      return input.value === this.name ? null : {notEqualsTo: true};
    };
  }

}
