import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-image-uploader',
  templateUrl: './image-uploader.component.html',
  styleUrls: ['./image-uploader.component.scss']
})
export class ImageUploaderComponent implements OnInit {

  @Output() blob: EventEmitter<Blob> = new EventEmitter<Blob>();
  @ViewChild('imageSelect') imageSelect: ElementRef;
  @Input() imageSrc = null;
  @Input() allowedExtensions = ['png', 'jpg'];
  invalidFiles = [];

  constructor() {
  }

  ngOnInit() {
  }

  onFilesChange(event) {
    this.submitFiles(event);
  }

  onInputChange(event) {
    this.submitFiles(event.target.files);
  }

  openDialog(event) {
    event.preventDefault();
    event.stopPropagation();
    this.imageSelect.nativeElement.click();
  }

  removeImage() {
    this.blob.emit(null);
  }

  private submitFiles(files: FileList) {
    if (files.length > 0) {
      Object.values(files).forEach((file) => {
        const ext = file.name.split('.')[file.name.split('.').length - 1];
        if (this.allowedExtensions.lastIndexOf(ext) !== -1) {
          this.blob.emit(file);
        } else {
          this.invalidFiles.push(file.name);
        }
      });
    }
  }
}
