import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-quiz-create',
  templateUrl: './quiz-create.component.html',
  styleUrls: ['./quiz-create.component.scss']
})
export class QuizCreateComponent implements OnInit {
  group: FormGroup;

  constructor(private fb: FormBuilder, private modal: NgbActiveModal) {
  }

  ngOnInit() {
    this.group = this.fb.group({
      name: this.fb.control(null, [Validators.required])
    });
  }

  create() {
    if (this.group.valid) {
      this.modal.close(this.group.value);
    } else {
      this.group.get('name').markAsTouched();
    }
  }

  dismiss() {
    this.modal.dismiss();
  }

}
