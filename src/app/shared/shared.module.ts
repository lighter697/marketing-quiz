import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ImageUploaderComponent} from './components/image-uploader/image-uploader.component';
import {DndDirective} from './directives/dnd/dnd.directive';
import {ModalConfirmComponent} from './components/modal-confirm/modal-confirm.component';
import {NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { QuizCreateComponent } from './components/quiz-create/quiz-create.component';
import {ReactiveFormsModule} from '@angular/forms';
import { ModalDeleteComponent } from './components/modal-delete/modal-delete.component';

@NgModule({
    imports: [
        CommonModule,
        NgbModalModule,
        ReactiveFormsModule,
    ],
  declarations: [
    ImageUploaderComponent,
    DndDirective,
    ModalConfirmComponent,
    SpinnerComponent,
    QuizCreateComponent,
    ModalDeleteComponent,
  ],
  exports: [
    ImageUploaderComponent,
    SpinnerComponent,
  ]
})
export class SharedModule {
}
