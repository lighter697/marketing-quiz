import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {QuizModule} from './modules/quiz';

const routes: Routes = [
  {path: '', loadChildren: './modules/quiz/quiz.module.ts#QuizModule'},
];

@NgModule({
  imports: [
    QuizModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
